## Comandos Git

`git init` = Para inicializar un proyecto git en una carpeta

`git status` = Saber el estatus en que están los archivos

`git add index.html` = Agrega el archivo al stagement

`git add -A` = Agrega todos los archivos modificados

`git rm --cached index.html` = Lo quita del stagement antes de hacer el commit

`git rm -f index.css` = Quitar del stagement y borrar el archivo

`git commit -m` = Hace un commit con los archivos agregados

> Agregar archivos dentro de un commit ya existente

- git add imagen.jpg
- git commit --amend

>Si hiciste un commit y te falto agregar al stagement otro archivo, para adjuntar los otros archivos únicamente hacemos esto.
Si le quieres modificar el mensaje del commit le pones al final -m "mensaje"

`git tag -a 0.5 -m 'mensaje'` = Para poner una versión al proyecto , en este caso le pusimos al último commit

`git tag -l` = Para enlistar todas las etiquetas

`git tag 0.3 sha1` = Para etiquetar un commit anterior

`git tag -d 1.0` = Elimina un tag indicando la versión a eliminar

`git tag -f -a 0.1 -m 'mensaje' sha1` = Renombra el tag de un commit

`git log --oneline` = Para mostrar los commit de una sola línea

`git log --oneline --graph` = Para mostrar los commit de una sola línea y saber su navegación entre ramas

`git log -2` = Te muestra los últimos 2 commit

`git diff [version 1 - vieja] vs [version 2 - reciente]` = Para comparar entre dos archivos, también se puede estar en el último commit y solo poner el archivo a comparar:

>git diff sha1 , la version 1 y version 2 significa los Sha de los archivos.

`git cherry-pick [Sha 1]` = Te ayuda a sacar un commit de una rama para ingresarla a la otra, esto es para cuando se cometen errores y hacer un commit en la Rama equivocada.

`git log --all` = Te muestra todo lo de un log

El arbolito de freddy (Log)

`git log --all --graph --decorate --oneline`

Para guardar este comando tan largo es con un alias que es parte de linux: (El comando que quieres guardar lo encierras en comillas)

`alias arbolito = “git log --all --graph --decorate --oneline”`

Y ya solo escribes `arbolito` y listo

-------------

### Ramas

`git branch nombreRama` = Crea una rama nueva

`git branch -l` = Enlistar todas las ramas

`git show-branch` = Muestra todas las ramas con sus historias de commits.

`git show-branch --all` = Lo mismo del anterior pero con más detalles

`git branch -d nombreRama` = Elimina la rama indicada

`git branch -m ramaAnterior ramaNueva` = Renombra una rama existente

`git checkout [Rama] [Sha 1]` = nos podemos mover entre ramas o entre commits (moviéndose en el tiempo)

`git checkout -b ramaNueva` = Para crear una nueva rama y moverse a esta nueva rama

>Para mezclar ramas una vez de que hayas hecho cambios, te posicionas a las ramas dónde quieres que se efectúen los cambios y de hacer el siguiente comando:

`git merge nombreRama`

`git push origin ––delete old-name` = elimina una rama remota ( que esta ubicada en Github o Gitlab ).

`git push origin –u new-name` = subir una nueva rama a remoto.

-------------

### Stash and Rebase

`git rebase` = Es un comando que sirve para darle organización a las ramas que has juntado con las demás ramas , cuando se hace un git log --oneline --graph , te muestra la historia del recorrido de estos cambios entre ramas.

`git stash` = Te ayuda a guardar los archivos que has modificado temporalmente, por ejemplo has modificado archivos para agregar nuevos features y requieres de arreglar bugs, entonces para no mover el archivo que modificaste a otra rama, lo guardas temporalmente.

`git stash drop [stash]` = Te ayuda a quitar los que guardaste en el stash.

`git stash list` = Enlista todos los cambios guardados temporalmente.

>Para guardar los cambios temporalmente se necesita ejecutar el comando git add [archivo] o git add -A

-------------

### Los 3 estados de Git
Working Directory: Es donde trabajamos de manera local, pero para guardarlo hay que pasarlo al Staging Area.

Staging Area: Es el área de preparación, se almacenan justo antes de hacer commit.

git repository: El repositorio donde almacenaremos los cambios del proyecto.

Tipos de sistemas control de versiones: VCS – sistema de control de versiones

Local: Vive en nuestro computador. Si le ocurre una catástrofe vamos a perder nuestro trabajo, además de que Ctrl + Z tiene cierta limitación | Solo vive en nuestro computador.

Centralizado: Depende de un super server donde está almacenado el repositorio. Si se quema el super server vamos a llorar y perdemos nuestro trabajo. | No depende únicamente de un computador en el que se trabaja, sino que depende del súper servidor en donde se almacena la información. El servidor provee las copias a sus hijos, pero solo guarda los cambios en un solo lugar
Distribuido: Cada participante del repositorio tiene una copia local y no afecta el trabajo del resto. No perdemos nuestro trabajo si nuestro se computador se daña, simplemente pedimos una copia a cualquier servidor donde esté almacenada la copia. | Cada uno de los que participan en el proyecto, tienen copia del proyecto que se realiza, por eso no dependemos de un solo computador que almacena toda la información.

>gitk: Te muestra en una interfaz el log. Esto ya viene predefinido cuando instalas Git.

-------------

### Rebase

El comando rebase sirve para fusionar dos ramas como el merge, la diferencia es que con rebase no los fusiona ordenadamente como lo hace el merge, usar rebase es una muy mala practica y solo se usa en local, en productivo es mala idea. Es mala idea por que no puedes ver quien hizo el commit, cuando lo hicieron, etc. En local es bueno por que no te importa todo eso y solo quieres unir tus cambios con la rama asignada a tu área local. Esto es para integrarse a la historia de tu repositorio local.
Rebase solo sirve para reescribir la historia del repositorio y solo debería hacerse en local.
Ejemplo
Quieres arreglar un bug y creas una rama la cual es hotfix, cuando fusionas los cambios estás añadiendo los cambios a tu repositorio local mas es mala práctica hacerlo con repositorios remotos.

Para hacer esto, te posicionas en la rama hotfix, y le das el comando: 

`git rebase master`

Con esto le dices que te pegue la historia a la rama master.
Y despues le haces rebase a la rama principal:

`git rebase experimento`

>NOTA: Primero se le hace rebase a la rama que quieres desaparecer (experimento) y después hacer el rebase a la rama principal (master).
Si no se siguen estos pasos, se podría entrar en un conflicto grande donde solo se puede arreglar con git reset.

>Un rebase es hacer cambios silenciosos en otras ramas donde no quieres ver la historia de estos cambios y pegar la historia de esa rama a otra rama.
En el mundo real el rebase lo que hace es ajustar donde ocurrió el primer commit haciendo un merge desde el primero y no desde el último.

Problemas:
- No queda historial
- No se sabe realmente quien fue quien hizo que
- En ocasiones, si la rama master avanzó mucho en commits puede crear muchos conflictos 

-------------

### Git Stash
Es un comando que sirve para guardar tus cambios temporalmente, así te puedes mover entre ramas sin hacer commit ya que no te puedes mover entre ramas si hay modificaciones en una. Podría decirse que “git stash” es el CTRL + Z en Git.  Lo que hace es que cuando ejecutas el comando regresa un commit anterior pero guardando tus cambios:

`git stash`

Si deseas ver los cambios que hiciste puedes hacer:

`git stash pop`

Si deseas eliminar estos cambios temporales:

`git stash drop`

Si deseas enlistar los cambios que hiciste:

`git stash list`

Puedes poner tu stash en otra rama, para hacerlo es de la siguiente manera:

`git stash branch nuevaRama`

De esa manera se creará una nueva rama con los nuevos cambios que hiciste.

Explicación Freddy:

Cuando necesitamos regresar en el tiempo porque borramos alguna línea de código pero no queremos pasarnos a otra rama porque nos daría un error ya que debemos pasar ese “mal cambio” que hicimos a stage, podemos usar git stash para regresar el cambio anterior que hicimos.
git stash es típico cuando estamos cambios que no merecen una rama o no merecen un rebase si no simplemente estamos probando algo y luego quieres volver rápidamente a tu versión anterior la cual es la correcta.

-------------

### Logs Avanzados

`git shortlog` = Te dice todos los commit que hizo cada persona en el proyecto

`git shortlog -sn` = Te dice las personas que han hecho commit en el proyecto y cuantas veces lo hicieron

`git shortlog -sn --all` = Te muestra lo mismo del anterior pero te muestra hasta los commits que fueron borrados.

`git shortlog -sn --all --no-merges` = Lo mismo que el anterior pero te omite los merges que se han hecho.

-------------

### Configuraciones
Configuraciones tu cuenta en tu computadora:

`git config --global user.email giannialvaradoc@gmail.com`

`git config --global user.name Gianni`

`git config --global color.ui true`

Para crear un nuevo comando llamado “stats” en git es de la siguiente manera:

`git config --global alias.stats “git shortlog -sn --all --no-merges”`

y después ejecutamos esto:

`git stats`

Y listo de esta manera creaste un nuevo comando, como el alias en Linux pero dentro de GIT.


#### Github Signup Verify

Se instalo este programa `Gpg4win (3.1.12)` y `GNU Privacy Guard - The Gnu Project` para generar la firma en cada commit y asi mostrar como `Verify` en los commits.

Pasos:

- Abre el programa `Kleopatra`
- En el menu `Archivo` -> `Nuevo par de claves...`
- sigue los pasos, no olvidar agregar la opcion para la contraseña
- Una vez que se tenga el certificado, selecciona tu certificado y dale en exportar, copia todo el contenido desde el inicio hasta el final.

Pasos en la consola git:

Nuestro ID seria este: `1752 B15D 5894 4Y52` pero todo deberia ir junto.

- git config --global user.signingkey 1752B15D58944Y52
- git config --global commit.gpgsign true
- git config --global tag.gpgsign true -- (Para los tags)
- git config --global gpg.program "C:\Program Files (x86)\GnuPG\bin\gpg.exe"

> La ruta es depende de donde se encuentre tu archivo .exe

y eso seria todo por parte de la consola.

Pasos en Github y Gitlab:

**Github**

- Selecciona tu perfil en la parte derecha de arriba
- Selecciona la opcion Settings
- Ve a la seccion SSH and GPG keys
- En la seccion de GPG keys agregas lo que copiaste del inicio al fin

**Gitlab**

- Selecciona tu perfil en la parte derecha de arriba
- Selecciona la opcion Preferences
- Ve a la seccion de GPG Keys
- Agregas lo que copiaste en exportar, de inicio a fin

> Es posible que te pida contraseña o no cuando se hace un commit, de eso si no se muy bien.

[Tutorial que segui para su uso](https://www.ankursheel.com/blog/securing-git-commits-windows)

-------------

### Extras

`git blame index.html` = Para saber quien hizo que en un archivo. Es como el Gitlens en Visual Studio Code.

`git blame -c index.html` = Para que idente un poco mejor y ver como funcionan todo este sistema.

`git blame --help` = Para obtener la ayuda de un comando, como en este caso es con blame.

`git blame index.html -L35,53` = Te dice quien hizo que desde cierta linea de codigo hasta donde. (inicio a fin).

`git blame index.html -L35,53 -c` = Lo mismo del anterior pero se agrega más formato.

`git branch -r` = Puedes ver las ramas remotas (En el repo en tu servidor)

`git branch -a` = Puedes ver todas las ramas (Locales y remotas)

`git push origin cabecera` = Para enviar tus ramas a tus repositorios remotos.

>En el video “Comandos y recursos colaborativos en Git y Github” en el minuto 5:02 explican más acerca de github y sus features.

-------------

### Git remote

###### Normalmente como lo hago:

Si deseas vincular tu proyecto local con uno remote que tienes en github, recomendación es mejor crear primero el proyecto en github, una vez creado clonas el proyecto a local y copias los archivos git que estan ocultos junto con el readme y pones esos datos al proyecto que quieras subir.

###### Como se deberia hacer:

- Crear proyecto en linea (Gitlab, Github)

- Crear proyecto en local y inicializar git `git init`

`git remote add origin git@gitlab.com:challenge-code/test-git.git` = en origin es el nombre que le quieras dar pero por defecto lo dejo en origin

`git remote set-url origin git@gitlab.com:challenge-code/test-git.git` = para editar la url

`git remote -v` = Para saber la configuracion que esta ligada al proyecto

`git fetch origin master` = para hacer fetch con el proyecto remote

`git merge origin/master` = para hacer el merge con los archivos

> Si te sale algun error un problema es por que se hicieron commits que no estan relacionados entre sí y por eso da problema al hacer el merge, el flag –allow-unrelated-histories solo lo debes poner cuando te salga el error

`git merge origin/master --allow-unrelated-histories` = Para arreglar el problema

-------------

### Configuraciones de llaves SSH

En este caso vamos a configurar las llaves ssh para una computadora nueva. En primera antes que nada tus configuraciones de git en tu computadora ya tienen que estar lista, para configurarlas te vas la sección de `Configuraciones`

>Recuerda estar en user/gianni/ para guardarlas llaves, tambien puedes consultar la ruta con: pwd

Una vez este hecha la configuración nos vamos a crear las llaves con este comando:

`ssh-keygen -t rsa -b 4096 -C "giannialvaradoc@gmail.com"`

Despues de crear la llave se copia el contenido del archivo `id_rsa.pub` y se publica en gitlab para crear registro de tu llave publica.

-------------

### Squash Commits

La definicion de squash commits se refiere como a juntar los commits que queramos en uno solo, esto para evitar llenar el historial de cambios que no son necesarios ver el registro del historial de commits y mejor los juntas todos en uno solo.

Para comenzar se agrega el comando rebase:

`git rebase -i HEAD~5`

El 5 significa cuantos commits quieres agrupar.

> En este caso son los ultimos 5 commits

Despues te aparece en la consola los commits, en ellos debes de sustituir la palabra `pick` por `s`, ejemplo:

`pick <hash> mensaje commit 1`

`s <hash> mensaje commit 2`

`s <hash> mensaje commit 3`

`s <hash> mensaje commit 4`

`s <hash> mensaje commit 5`

Para guardar los cambios:
1. `esc`
1. `:w`
1. `:x`

Luego te aparece otra ventana donde aparecen los commits, ahi solo pones `#` para comentar los commits que no quieres que aparescan en el unico commit que quieres, ademas de que puedes editar el mensaje.

> Como consejo para poder editar en la consola, presiona la tecla `s`.

Para guardar los cambios:
1. `esc`
1. `:w`
1. `:x`

Despues ya envias los cambios a tu repositorio con el comando:

`git push -f`

-------------

### Buenas practicas en los commits

Buenas practicas para tener una mejor descripcion en los mensajes de los commits:

- feature: nueva caracteristica agregada
- fix: se soluciono un bug / typo
- chore: actualizacion de tareas build
- test: se agregan pruebas unitarias pero sin cambiar el codigo
- style: se agregaron estilos
- refactor: refactorizacion del codigo
